<!--Please use Emojicopy (https://www.emojicopy.com) to insert Emoji's.-->

# 💯About this Mod Menu💗

Mod Menu For [Dying Light](https://dyinglightgame.com/dying-light/)

- No Crash Items

- [FPS](https://www.nexusmods.com/dyinglight/mods/865?tab=files) or No FPS

- Up To Date

#

<details><!--Installation-->
<summary><h2>📄 Installation 📁</h2></summary>

-  Open your Dying Light Directory, then open DW_DLC24

-  Drag the pak into the DW_DLC24 folder

- Rename the pak so it says DataDLC24_2.pak

- Then your done!

#
</details><!--Installation-->

<details><!--Credit-->
<summary><h2>💻 Credit 📱</h2></summary>

- [Inari](https://gitlab.com/altyinari) (Creator of this Mod Menu)

#
</details><!--Credit-->

<details><!--Patch Notes-->
<summary><h2>📫 Patch Notes 📓</h2></summary>

<details><!--Patch Notes::Version 9-->
<summary><h2>Version 9</h2></summary>

+ Minor change to mod menu apperance

+ Fixed menu hud

+ Changed view distance of FPS file

#
</details><!--Patch Notes::Version 9-->

<details><!--Patch Notes::Version 8-->
<summary><h2>Version 8</h2></summary>

+ Updated Inventory files

#
</details><!--Patch Notes::Version 8-->

<details><!--Patch Notes::Version 7-->
<summary><h2>Version 7</h2></summary>

+ Updated all paks

+ Added img of mod menu to site

#
</details><!--Patch Notes::Version 7-->

<details><!--Patch Notes::Version 6-->
<summary><h2>Version 6</h2></summary>

+ Updated DataDLC24_2_FPS_.pak

+ Deleted useless collectables_dlc9.scr

#
</details><!--Patch Notes::Version 6-->

<details><!--Patch Notes::Version 5-->
<summary><h2>Version 5</h2></summary>

+ New way of adding mod menu

#
</details><!--Patch Notes::Version 5-->

<details><!--Patch Notes::Version 4-->
<summary><h2>Version 4</h2></summary>

+ Got Rid of A Secret Thing

#
</details><!--Patch Notes::Version 4-->

<details><!--Patch Notes::Version 3-->
<summary><h2>Version 3</h2></summary>

+ Added Inventory to Mod Menu

#
</details><!--Patch Notes::Version 3-->

<details><!--Patch Notes::Version 2-->
<summary><h2>Version 2</h2></summary>

+ Complete Overhaul of Dev Menu Apperance

#
</details><!--Patch Notes::Version 2-->

<details><!--Patch Notes::Version 1-->
<summary><h2>Version 1</h2></summary>

+ Got Rid of Modded Hud

+ Added DLC into Items

+ Changed Apperance of Mod Menu

#
